<h1 align="center">
  Gatsby Blog
</h1>

## 🦾 Built with
* Gatsby
* GitLab
* Netlify CMS

## 🚀Quick start
  Clone or download the repo. Run yarn to install all the packages:
   ```shell
   yarn
  ```

  Also make sure you have the Gatsby CLI globally installed:
  ```shell
  npm install -g gatsby-cli
  ```

  ## GitLab CI/CD

  Change site meta data (for CI/CD)
  ```shell
  siteMetadata: {
    title: `Dide's blog`,
    description: `This is Dide's personal blog.`,
    author: `@gatsbyjs`,
    pathPrefix: `/personal-blog`
  },
  ```

  For more info, click [here](https://www.gatsbyjs.org/docs/deploying-to-gitlab-pages/).

  ### Netlify CMS:
  To use the Netlify CMS, you'll have to add your application in GitLab. You can find more info [here](https://www.gatsbyjs.org/docs/sourcing-from-netlify-cms/). 

  Don't forget to change the repo in `static/admin/config.yml`. Also change the app_id. You can find this id under settings > applications on GitLab.

  ```shell
  backend:
  name: gitlab
  repo: didemertens/personal-blog // change this
  auth_type: implicit
  app_id: ec8147350b91539ff436caaf72a1280b8b5c5b9e65c70e46c14ad52ba198c27a // and change this
  ...
  ```

  ### Run the server:
  ```shell
  gatsby develop
  ```
  Your site is now running at `http://localhost:8000`! 🥳

  >> and deployed
  `https://YOUR_GITLAB_USERNAME.gitlab.io/NAME_OF_YOUR_REPO`



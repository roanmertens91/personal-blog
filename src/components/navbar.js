import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"

import Image from "../components/image"

const ImageWrapper = styled.div`
  width: 100px;
  margin: 8px 20px;
`

const NavBar = () => (
  <nav
    style={{
      display: `flex`,
      justifyContent: `space-between`
    }}
  >
    <div>
      <Link
        to="/"
      >
        <ImageWrapper>
          <Image />
        </ImageWrapper>
      </Link>
    </div>
    <div>
      <Link
        to="/posts"
        style={{
          color: `#FFA502`,
          textDecoration: `none`,
          margin: `1rem`,
          fontSize: `1.2rem`
        }}
      >
        Posts
    </Link>
      <Link
        to="/about-me/"
        style={{
          color: `#FFA502`,
          textDecoration: `none`,
          margin: `1rem`,
          fontSize: `1.2rem`
        }}
      >
        About Me
    </Link>
    </div>
  </nav>
)

export default NavBar

import React from "react"
import styled from "styled-components"

import NavBar from "./navbar"

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  min-height: 100vh;
  min-width: 100vw;
`

const Footer = styled.footer`
  text-align: center;
  width: 100%;
  padding: 5px 0;
`

const Layout = ({ children }) => (
  <Container>
    <NavBar />
    <main>{children}</main>
    <Footer>
      © {new Date().getFullYear()}, Built with
      {` `}
      <a className="a--orange" href="https://www.gatsbyjs.org">Gatsby</a>
    </Footer>
  </Container>
)

export default Layout

import React from "react"
import { graphql } from "gatsby"
import PostLink from "../components/post-link"

import Layout from "../components/layout"
import SEO from "../components/seo"

const PostsPage = ({
  data: {
    allMarkdownRemark: { edges },
  },
}) => {
  const Posts = edges
    .filter(edge => !!edge.node.frontmatter.date)
    .map(edge => <PostLink key={edge.node.id} post={edge.node} />)
  return (
    <Layout>
      <SEO title="Blogs" />
      <header className="text--centered">
        <h1>Blog posts</h1>
      </header>
      <ul>
        {Posts}
      </ul>
    </Layout>
  )
}

export default PostsPage

export const pageQuery = graphql`
  query {
    allMarkdownRemark(
      filter: {frontmatter:{path: {ne: null }}},
      sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          id
          frontmatter {
            date(formatString: "DD MMMM, YYYY")
            path
            title
          }
        }
      }
    }
  }
`


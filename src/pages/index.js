import React from "react"
import { graphql, Link } from "gatsby"

import PostLink from "../components/post-link"
import Layout from "../components/layout"
import SEO from "../components/seo"

const IndexPage = ({
  data: {
    allMarkdownRemark: { edges },
  }
}) => {
  const Post = edges
    .map(edge => <PostLink key={edge.node.id} post={edge.node} />)

  return (
    <Layout>
      <SEO title="Home" />
      <header className="text--centered">
        <h1>Welcome!</h1>
      </header>
      <p>I'm Dide (pronounced: di-duh) and this is my very own Digital Garden.</p>
      <p>It's a place where I save my notes and thoughts on various topics, 
        ranging from building websites to the most delicious miso soup.</p>
      <p>Lorem ipsum dolor sit amet, eum nullam numquam docendi at. Te virtute 
        suavitate sit, malorum utroque mei an. Qualisque repudiandae.</p>
      <div className="text--centered">
        <Link 
        to="/posts/"
        className="a--orange" 
        >
          Go to posts</Link>
      </div>
      <h3>Newest post</h3>
      <ul>
        {Post}
      </ul>
    </Layout>
  )
}


export default IndexPage

export const pageQuery = graphql`
  query {
    allMarkdownRemark(
      limit: 1, 
      filter: {frontmatter:{path: {ne: null }}},
      sort: { order: DESC, fields: [frontmatter___date] }) {
      edges {
        node {
          id
          frontmatter {
            date(formatString: "DD MMMM, YYYY")
            path
            title
          }
        }
      }
    }
  }
`
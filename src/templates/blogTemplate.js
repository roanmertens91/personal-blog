import React from "react"
import { graphql } from "gatsby"

import Layout from "../components/layout"

export default function Template({
  data,
}) {
  const {blogText: {frontmatter: {title, date }} } = data
  const {blogAuthor: {frontmatter: {name, about }} } = data
  const {blogText: { html }} = data

  return (
    <Layout>
      <div className="blog-post-container">
        <article className="blog-post">
          <h1>{title}</h1>
          <time>{date}</time>
          <p>{name}</p>
          <p>{about}</p>
          <section
            className="blog-post-content"
            dangerouslySetInnerHTML={{ __html: html }}
          />
        </article>
      </div>
    </Layout>
  )
}

export const pageQuery = graphql`
  query BlogDetails($slug: String!, $author: String!) {
    blogText: markdownRemark(frontmatter: {path: {eq: $slug }}) {
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
      }
    }
    
    blogAuthor: markdownRemark(frontmatter: {name: {eq: $author }}) {
      frontmatter {
        name
        about
      }
    }
  }
`;